console.log("Hello Alpha")

//Insert Documents (Create)

//Syntax
    //db.collectionName.insertOne({object});
    //db.collectionName.insert({Object});
//JavaScript syntax comparison
    //Object.object.method({object});

    db.users.insert({
        firstName:"Jane",
        lasetName:"Doe",
        age: 21,
        contact: {
            phone: "0987654321",
            email: "janedoe@gamil.com"
        },
        courses: ["CSS","JavaScript","Phython"],
        department: "none"
    });

//Insert Many

    //Syntax
        //db.collectionName.insertMany([{objectA},{objectB}]);

    db.users.insertMany([
        {
            firstName: "Stephen",
            lastName: "Hawking",
            age: 76,
            contact:{
                phone: "0987654321",
                email: "stephenhawking@gmail.com"
            },
            courses: ["CSS","JavaScript","Phython"],
            department: "none"
        },
        {
            firstName: "Neil",
            lastName: "Armstrong",
            age: 82,
            contact:{
                phone: "0987654321",
                email: "neilarmsteong@gmail.com"
            },
            courses: ["React","Laravel","Sass"],
            department: "none"  
        }

    ]);


    //Finding Documents (Read/Retrieve)

    //Syntax
    //db.collectionName.find();
    //bd.collectionName.finde({field:value});

    db.users.find()
    db.users.find({firstName:"Stephen"});

    //db.collectionName.find({fieldA: valueA, fieldB: valueB});

    db.users.find({lastName: "Armstrong", age: 82});

    //Update Documents

    /***create */
    db.users.insert({
        firstName:"Test",
        lasetName:"Test",
        age: 0,
        contact: {
            phone: "0000000000",
            email: "test@gamil.com"
        },
        courses: [],
        department: "none"
    });

    //db.collectionName.updateOne({criteria},{$set: {field:value}});

    db.users.updateOne(
        { firstName: "Test" },
        { 
            $set: {
            firstName:"Bill",
            lasetName:"Gates",
            age: 0,
            contact: {
            phone: "12345678",
            email: "billgates@gamil.com"
            },
            courses: ["PHP","Laravel","HTML"],
            department: "active"
            }
        }
        );

        //Updating Multiple documents
        //db.collectionNAme.updateMany({criteria},{$set: {field:value}});

        db.users.updateMany(
            { department: "none" },
            { 
                $set: {
                department: "HR"
                }
            }
            );
        //ReplaceOne
            //db.collectionName.replaceOne({criteria},{{field:value}} )

            
            db.users.replaceOne(
                { firstName: "Bill" },
                { 
                    firstName:"Bill",
                lasetName:"Gates",
                age: 65,
                contact: {
                phone: "12345678",
                email: "bill@gamil.com"
                },
                courses: ["PHP","Laravel","HTML"],
                department: "operatins"
                }
            );

            //Deletind Documents

            db.users.insert({
                firstName:"Test",
            });

            //db.collectionName.deleteOne({criteria})
            db.users.deleteOne({
                firstName:"Test",
            });

            //db.collectionName.deleteMany({criteria})
            db.users.deleteMany({
                firstName:"Bill",
            });

            //Advance Queries

            db.users.find({
                contact:{
                    phone: "0987654321",
                    email: "stephenhawking@gmail.com"
                } 
            })

            db.users.find(
                {"contact.email": "janedoe@gamil.com"}
            )

            db.users.find({courses: ["CSS","javaScript","Python"]})
            db.users.find({courses: {$all:["React","Phython"]}})

            db.users.insert({
                namearr:[
                    {
                        name: "juan"
                    },
                    {
                        nameb: "tamad"
                    }
                ]
            })

            db.users.find({
                namearr: 
                {
                    name: "juan"   
                }
            })